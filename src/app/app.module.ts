import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ChartsModule } from 'ng2-charts';
// Components
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { SearchFormComponent } from './search-form/search-form.component';
// Services
import { SearchGithubService } from './search-github.service';
import { AppDataService } from './app-data.service';
import { ResultsComponent } from './results/results.component';
import { RepoDetailsComponent } from './repo-details/repo-details.component';
import { IssuesTabComponent } from './issues-tab/issues-tab.component';
import { LoadingGifComponent } from './loading-gif/loading-gif.component';
import { PieChartComponent } from './pie-chart/pie-chart.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SearchFormComponent,
    ResultsComponent,
    RepoDetailsComponent,
    IssuesTabComponent,
    LoadingGifComponent,
    PieChartComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ChartsModule
  ],
  providers: [
    SearchGithubService,
    AppDataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
