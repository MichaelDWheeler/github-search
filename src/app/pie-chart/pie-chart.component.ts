import { Component, OnInit } from '@angular/core';
import { AppDataService } from '../app-data.service';

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.css']
})
export class PieChartComponent implements OnInit {
  public pieChartLabels: string[] = ['Open Issues', 'Closed Issues'];
  public pieChartType = 'pie';

  constructor(public data: AppDataService) { }

  ngOnInit() {
  }

}
