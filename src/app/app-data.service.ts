import { Injectable } from '@angular/core';
import { Data } from './data';

@Injectable()
export class AppDataService {
  errorAlerted = false;
  navigationCount = 1;
  pageCount = 1;
  issuesCount = 0;
  issues: any = [];
  openIssues: any = [];
  closedIssues: any = [];
  repositoryDetail: any = {};
  repositories: any = [];
  selectedIssues: any = [];
  state = 'all';

  data: Data = {
    repositorySelected: false,
    showComponent: false,
    showIssues: false,
    showNext: true,
    showPrevious: true,
    fullName: '',
    loadGif: false,
    showRepo: false,
    openIssues: 0,
    closedIssues: 0
  };

  errorMessage(error) {
    this.data.loadGif = false;
    if (!this.errorAlerted) {
      this.errorAlerted = true;
      let message = '';
      if (error.status = 403) {
        message = 'You are making requests too quickly. You will need to wait 30 seconds and then try again.';
      }
      alert(`${message}\n\nError: Status ${error.status}, ${error.message}.`);
    }
    setTimeout(() => {
      this.errorAlerted = false;
    }, 3000);
  }

  resetCounts(): void {
    this.pageCount = 1;
    this.issuesCount = 0;
    this.navigationCount = 1;
  }

  resetVariables() {
    this.issues = [];
    this.openIssues = [];
    this.closedIssues = [];
    this.data.showRepo = false;
    this.data.loadGif = true;
  }

  selectTab($event): void {
    this.resetCounts();
    this.issuesCount = 0;
    if ($event === 'all') {
      this.state = 'all';
      this.selectedIssues = this.issues[0];
    } else if ($event === 'open') {
      this.state = 'open';
      this.selectedIssues = this.openIssues[0];
    } else if ($event === 'closed') {
      this.state = 'closed';
      this.selectedIssues = this.closedIssues[0];
    } else {
      return;
    }
  }

  constructor() { }

}
