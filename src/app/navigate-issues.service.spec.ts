import { TestBed, inject } from '@angular/core/testing';

import { NavigateIssuesService } from './navigate-issues.service';

describe('NavigateIssuesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NavigateIssuesService]
    });
  });

  it('should be created', inject([NavigateIssuesService], (service: NavigateIssuesService) => {
    expect(service).toBeTruthy();
  }));
});
