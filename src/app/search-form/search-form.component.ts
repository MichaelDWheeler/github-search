import { Component, OnInit } from '@angular/core';
import { SearchGithubService } from '../search-github.service';
import { AppDataService } from '../app-data.service';
import { ifError } from 'assert';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.css']
})
export class SearchFormComponent implements OnInit {
  public searchQuery: string;

  clearForm(): void {
    this.searchQuery = this.query.clearField(this.searchQuery);
  }

  search(): void {
    this.data.repositories = [];
    this.data.resetVariables();
    this.data.data.loadGif = false;
    this.data.data.repositorySelected = false;
    this.query.searchGithub(this.searchQuery)
      .subscribe(data => {
        this.data.repositories.push(data['items']);
        this.data.data.showComponent = true;
      },
        error => {
          this.data.errorMessage(error);
        }
      );
  }

  searchByEnter($event): void {
    if ($event.keyCode === 13) {
      this.search();
    }
  }

  constructor(
    public query: SearchGithubService,
    public data: AppDataService) { }

  ngOnInit() {
  }

}
