import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { SearchFormComponent } from './search-form.component';
import { FormsModule } from '@angular/forms';
import { SearchGithubService } from '../search-github.service';
import { AppDataService } from '../app-data.service';
import { HttpClientModule } from '@angular/common/http';

describe('SearchFormComponent', () => {
  let component: SearchFormComponent;
  let fixture: ComponentFixture<SearchFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SearchFormComponent],
      imports: [
        FormsModule,
        HttpClientModule,
      ],
      providers: [
        SearchGithubService,
        AppDataService
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should cause "searchQuery" to equal an empty string when calling clearForm method', () => {
    component.clearForm();
    expect(component.searchQuery).toBe('');
  });

  it('should push objects to array that contain "angular"', inject([SearchGithubService, AppDataService], (service: SearchGithubService, appService: AppDataService) => {
    component.searchQuery = 'angular';
    component.search();
    const checkLength = () => {
      if (appService.repositories.length = 0) {
        const wait = window.setTimeout(() => {
          checkLength();
        }, 100);
      } else {
        expect(appService.repositories.length).toBe(30);
        expect(appService.repositories).toContain('angular');
      }
    };
  }));

  it('calls the search() method when the keyCode for Enter is submitted', () => {
    const $event: any = {};
    $event.keyCode = 13;
    spyOn(component, 'search');
    component.searchByEnter($event);
    expect(component.search).toHaveBeenCalled();
  });

});
