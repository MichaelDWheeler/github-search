import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AppDataService } from '../app-data.service';
import { SearchGithubService } from '../search-github.service';

@Component({
  selector: 'app-issues-tab',
  templateUrl: './issues-tab.component.html',
  styleUrls: ['./issues-tab.component.css']
})
export class IssuesTabComponent implements OnInit {
  @Input() tabSelect: string;
  @Output() tabShown = new EventEmitter();
  tabShowing: string;

  issues(): void {
    if (this.tabSelect === 'All') {
      this.tabShown.emit('all');
      this.tabShowing = 'All';
    } else if (this.tabSelect === 'Open') {
      this.tabShown.emit('open');
      this.tabShowing = 'Open';
    } else if (this.tabSelect === 'Closed') {
      this.tabShown.emit('closed');
      this.tabShowing = 'Closed';
    } else {
      this.tabShown.emit('all');
      this.tabShowing = 'All';
    }
  }

  constructor(
    public data: AppDataService,
    public query: SearchGithubService
  ) { }

  ngOnInit() {
    this.tabShowing = 'All';
  }

}
