import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IssuesTabComponent } from './issues-tab.component';
import { AppDataService } from '../app-data.service';
import { SearchGithubService } from '../search-github.service';
import { HttpClient, HttpHandler } from '@angular/common/http';

describe('IssuesTabComponent', () => {
  let component: IssuesTabComponent;
  let fixture: ComponentFixture<IssuesTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IssuesTabComponent ],
      providers: [
        AppDataService,
        SearchGithubService, 
        HttpClient,
        HttpHandler
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IssuesTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
