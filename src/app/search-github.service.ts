import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Count } from './count';
import { Issues } from './issues';
import { Repositories } from './repositories';
import { Observable } from 'rxjs/Observable';
import { AppDataService } from './app-data.service';

@Injectable()
export class SearchGithubService {
  private _url = 'https://api.github.com/search/repositories?q=';
  private _issuesUrl = 'https://api.github.com/repos/';
  private _detailIssues = 'https://api.github.com/search/issues?q=repo:';

  clearField(val): string {
    return val = '';
  }

  getIssues(name): void {
    this.data.resetVariables();
    this.searchIssues(name, 'all', 90, 1)
      .subscribe(data => {
        this.data.issues.push(data['items']);
        if (this.data.issues[0].length > 0) {
          this.data.selectTab('all');
          this.data.data.showRepo = true;
          this.data.data.showIssues = true;
          this.getIssuesCount(name);
        } else {
          this.data.data.showRepo = true;
          this.data.data.showIssues = false;
        }
        this.data.data.loadGif = false;
      },
        error => {
          this.data.errorMessage(error);
        });
  }

  getIssuesCount(name) {
    this.data.data.openIssues = 0;
    this.data.data.closedIssues = 0;
    this.searchOpenIssues(name)
      .subscribe(data => { this.data.data.openIssues = data['total_count']; },
        error => {
          this.data.errorMessage(error);
        });
    this.searchClosedIssues(name)
      .subscribe(data => { this.data.data.closedIssues = data['total_count']; },
        error => {
          this.data.errorMessage(error);
        });
  }

  showDetails(item): void {
    this.data.resetVariables();
    this.data.resetCounts();
    this.data.repositoryDetail = item;
    this.data.data.repositorySelected = true;
    this.data.data.fullName = item['full_name'];
    this.getIssues(item['full_name']);
  }

  getRepoDetails(item): any {
    return item;
  }

  searchGithub(query): Observable<Repositories[]> {
    return this.http.get<Repositories[]>(this._url + query + `&per_page=50`);
  }

  searchIssues(repo, state, perPage, page): Observable<Issues[]> {
    let status: string;
    if (state === 'all') {
      status = '';
    } else {
      status = `+state:${state}`;
    }
    return this.http.get<Issues[]>(this._detailIssues + repo + `+type:issue${status}&per_page=${perPage}&page=${page}`);
  }

  searchOpenIssues(repo) {
    return this.http.get<Count[]>(this._detailIssues + repo + `+type:issue+is:open`);
  }

  searchClosedIssues(repo) {
    return this.http.get<Count[]>(this._detailIssues + repo + `+type:issue+is:closed`);
  }

  constructor(
    private http: HttpClient,
    public data: AppDataService
    ) { }

}
