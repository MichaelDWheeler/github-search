export interface Data {
    repositorySelected: boolean;
    showComponent: boolean;
    showIssues: boolean;
    showNext: boolean;
    showPrevious: boolean;
    fullName: string;
    loadGif: boolean;
    showRepo: boolean;
    openIssues: number;
    closedIssues: number;
}
