import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { SearchFormComponent } from './search-form/search-form.component';
import { FormsModule } from '@angular/forms';
import { SearchGithubService } from './search-github.service';
import { HttpClientModule } from '@angular/common/http';
import { AppDataService } from './app-data.service';
import { ResultsComponent } from './results/results.component';
import { RepoDetailsComponent } from './repo-details/repo-details.component';
import { LoadingGifComponent } from './loading-gif/loading-gif.component';
import { PieChartComponent } from './pie-chart/pie-chart.component';
import { IssuesTabComponent } from './issues-tab/issues-tab.component';
import { ChartsModule } from 'ng2-charts';

describe('AppComponent', () => {
    beforeEach(async(() => {
      TestBed.configureTestingModule({
        declarations: [
          AppComponent,
          HeaderComponent,
          SearchFormComponent,
          ResultsComponent,
          RepoDetailsComponent,
          LoadingGifComponent,
          PieChartComponent,
          IssuesTabComponent
        ],
        imports: [
          FormsModule,
          HttpClientModule,
          ChartsModule
        ],
        providers: [
          SearchGithubService,
          AppDataService
        ]
      }).compileComponents();
    }));
    it('should create the app', async((done) => {
      const fixture = TestBed.createComponent(AppComponent);
      const app = fixture.debugElement.componentInstance;
      expect(app).toBeTruthy();
    }));
    it('should render title in a h1 tag', async((done) => {
      const fixture = TestBed.createComponent(AppComponent);
      fixture.detectChanges();
      const compiled = fixture.debugElement.nativeElement;
      expect(compiled.querySelector('h1').textContent).toContain('Search Github');
    }));
  });
