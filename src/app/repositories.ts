export interface Repositories {
    html_url: string;
    description: string;
    forks: string;
    count: number;
    stargazersCount: number;
    openIssues: number;
}
