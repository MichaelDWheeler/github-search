import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { ResultsComponent } from './results.component';
import { AppDataService } from '../app-data.service';
import { SearchGithubService } from '../search-github.service';
import { HttpClientModule } from '@angular/common/http';

describe('ResultsComponent', () => {
  let component: ResultsComponent;
  let fixture: ComponentFixture<ResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ResultsComponent],
      providers: [
        AppDataService,
        SearchGithubService
      ],
      imports: [ HttpClientModule ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should push objects to array that contain "angular"', inject([SearchGithubService], (service: SearchGithubService) => {
    const searchQuery = 'testing';
    service.searchGithub(searchQuery);
    const checkLength = () => {
      if (component.repositories.length = 0) {
        const wait = window.setTimeout(() => {
          checkLength();
        }, 100);
      } else {
        expect(component.repositories.length).toBe(30);
        expect(component.repositories).toContain('testing');
      }
    };
  }));
});
