import { Component, OnInit } from '@angular/core';
import { AppDataService } from '../app-data.service';
import { SearchGithubService } from '../search-github.service';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {
  
  constructor(
    public data: AppDataService,
    public query: SearchGithubService
  ) { }

  ngOnInit() {}
}
