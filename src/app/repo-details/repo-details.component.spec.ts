import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RepoDetailsComponent } from './repo-details.component';
import { AppDataService } from '../app-data.service';
import { LoadingGifComponent } from '../loading-gif/loading-gif.component';
import { PieChartComponent } from '../pie-chart/pie-chart.component';
import { IssuesTabComponent } from '../issues-tab/issues-tab.component';
import { ChartsModule } from 'ng2-charts';
import { SearchGithubService } from '../search-github.service';
import { HttpClient, HttpHandler } from '@angular/common/http';

describe('RepoDetailsComponent', () => {
  let component: RepoDetailsComponent;
  let fixture: ComponentFixture<RepoDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        RepoDetailsComponent,
        LoadingGifComponent,
        PieChartComponent,
        IssuesTabComponent,
      ],
      providers: [
        AppDataService,
        SearchGithubService,
        HttpClient,
        HttpHandler
      ],
      imports: [
        ChartsModule,
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RepoDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
