import { Component, OnInit } from '@angular/core';
import { AppDataService } from '../app-data.service';
import { SearchGithubService } from '../search-github.service';

@Component({
  selector: 'app-repo-details',
  templateUrl: './repo-details.component.html',
  styleUrls: ['./repo-details.component.css']
})
export class RepoDetailsComponent implements OnInit {

  allIssues = 'All';
  closedIssues = 'Closed';
  openIssues = 'Open';

  newIssues: any = [];

  getClosedIssues(): void {
    if (this.data.closedIssues[0] === undefined || !this.data.closedIssues[0].length) {
      const name = this.data.data.fullName;
      this.query.searchIssues(name, 'closed', 90, 1)
        .subscribe(data => {
          this.data.closedIssues.push(data['items']);
          this.data.selectedIssues = this.data.closedIssues[0];
        },
        error => {
          this.data.errorMessage(error);
        });
    } else {
      this.data.selectedIssues = this.data.closedIssues[0];
    }

  }

  getOpenIssues(): void {
    if (this.data.openIssues[0] === undefined || !this.data.openIssues[0].length) {
      const name = this.data.data.fullName;
      this.query.searchIssues(name, 'open', 90, 1)
        .subscribe(data => {
          this.data.openIssues.push(data['items']);
          this.data.selectedIssues = this.data.openIssues[0];
        },
        error => {
          this.data.errorMessage(error);
        });
    } else {
      this.data.selectedIssues = this.data.openIssues[0];
    }
  }

  next(): string {
    if (this.data.selectedIssues !== undefined) {
      if (this.data.selectedIssues.length > (this.data.issuesCount + 30)) {
        return 'Next';
      }
    }
  }

  nextIssues(): void {
    this.newIssues = [];
    const name = this.data.data.fullName;
    this.data.navigationCount++;
    this.data.pageCount = (this.data.selectedIssues.length / 90) + 1;
    this.data.issuesCount += 30;
    if (this.data.selectedIssues.length <= (this.data.issuesCount + 30)) {
      this.query.searchIssues(name, this.data.state, 90, this.data.pageCount)
        .subscribe(data => {
          this.newIssues.push(data['items']);
          this.data.selectedIssues = this.data.selectedIssues.concat(this.newIssues[0]);
        },
        error => {
          this.data.errorMessage(error);
        });
    }
  }

  previous(): string {
    if (this.data.selectedIssues !== undefined) {
      if (this.data.navigationCount > 1) {
        return 'Previous';
      }
    }
  }

  previousIssues(): void {
    this.data.navigationCount--;
    this.data.issuesCount -= 30;
  }

  constructor(
    public data: AppDataService,
    public query: SearchGithubService
  ) { }

  ngOnInit() { }
}
