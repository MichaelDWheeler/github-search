import { TestBed, inject } from '@angular/core/testing';

import { SearchGithubService } from './search-github.service';
import { HttpClientModule } from '@angular/common/http';

describe('SearchGithubService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SearchGithubService],
      imports: [HttpClientModule]
    });
  });

  it('should be created', inject([SearchGithubService], (service: SearchGithubService) => {
    expect(service).toBeTruthy();
  }));
});
