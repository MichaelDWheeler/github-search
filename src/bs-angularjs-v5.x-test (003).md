

**The Test**

Create a JavaScript / TypeScript application using Angular 5 (Using Angular CLI) that will allow a user to connect to GitHub API's and 
allow the user to navigate the app which contains some features as itemised below. 
We will assess your deliverable based on your code neatness, readability, logical flow, visual design and speed. 
All individuals have different strengths so be sure to choose the approach to problem solving that suite your strengths.

**Features:**

1)      As a User I would like to search github in order to view the available repositories for a given search term

2)      As a User I would like to select a particular repository in order to view more details of the selected repository

a.      URL, description, forks count, stargazers count, open issues count etc

3)      As a User I would like to link off to the actual GitHub page where the repository is located in order to view the code in the repository

4)      As a User I would like to view a list of all the current issues for a repository in order to view the backlog of issues

5)      As a User I would like to filter the list of issues between STATE = ["Open" or "Closed"] in order to look through the filtered list

6)      As a User I would like to view a PIE chart that displays the breakdown of issues for the repository (open vs closed) in order to visually see how well built and maintained the repository is



Additional Points and Notes:

- Implement the best design possible for the user interface
- You can use Twitter Bootstrap, Material and any other libraries to support AngularJS (Lodash etc)
- Use best practices in writing JavaScript / TypeScript, CSS, and HTML. Write clearly and use proper MVC structure to write the application.

**Relevant Links**

| **Description** | **URL** |
| --- | --- |
| Sample API URL to search by repository name | [https://api.github.com/search/repositories?q=bootstrap](https://api.github.com/search/repositories?q=bootstrap) |
| API URL to display issues of a repository name | [https://api.github.com/search/issues?q=repo:username/reponame](https://bitbucket.org/one-way/blackswan-tests/src/2bd0eb2fd2fa13c11651b102a6a86c6c53fa6110/bs-angularjs-test.md?at=master&amp;fileviewer=file-view-default) |
| Example: Display Issues of Bootstrap | [https://api.github.com/repos/twbs/bootstrap/issues?state=all](https://api.github.com/repos/twbs/bootstrap/issues?state=all) |
| API Documentation | [https://developer.github.com/v3/search/#search-issues](https://developer.github.com/v3/search/#search-issues) |
| GItHub Search API Documentation | [https://developer.github.com/v3/search/](https://developer.github.com/v3/search/) |

